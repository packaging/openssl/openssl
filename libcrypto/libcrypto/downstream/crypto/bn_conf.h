/* file      : libcrypto/downstream/crypto/bn_conf.h -*- C -*-
 * license   : Apache License 2.0; see accompanying LICENSE file
 */

#ifndef LIBCRYPTO_DOWNSTREAM_CRYPTO_BN_CONF_H
#define LIBCRYPTO_DOWNSTREAM_CRYPTO_BN_CONF_H

/* Include upstream's auto-generated platform-specific bn_conf.h.
 */
#include <crypto/bn_conf/platform.h>

#endif /* LIBCRYPTO_DOWNSTREAM_CRYPTO_BN_CONF_H */
