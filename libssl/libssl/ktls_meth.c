/* file      : libssl/ktls_meth.c
 * license   : Apache License 2.0; see accompanying LICENSE file
 */
#include <openssl/configuration.h>

#ifndef OPENSSL_NO_KTLS
#  include <ssl/record/methods/ktls_meth.c>
#endif
