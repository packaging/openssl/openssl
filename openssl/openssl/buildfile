# file      : openssl/buildfile
# license   : Apache License 2.0; see accompanying LICENSE file

import libs  = libssl%lib{ssl}
import libs += libcrypto%lib{crypto}

tclass = $c.target.class
tsys   = $c.target.system

windows = ($tclass == 'windows')

exe{openssl}: {h c}{** -vms_* -apps/lib/win32_init}
exe{openssl}: apps/lib/c{win32_init}: include = $windows
exe{openssl}: $libs

# Build options.
#
# Drop -DOPENSSL_BUILDING_OPENSSL as it is not used in the source code.
#
# Note that upstream also passes -DNDEBUG. Let's omit it for now to enable
# assertions to gain some extra confidence that we didn't break anything while
# packaging.
#
# Also note that openssl doesn't use compression libraries directly. However,
# it uses functionality that libcrypto additionally provides when zlib and
# zstd are enabled.
#
c.poptions += -DZLIB -DZSTD

if $windows
  c.poptions += -DWIN32_LEAN_AND_MEAN -DUNICODE -D_UNICODE

switch $tclass, $tsys
{
  case 'windows', 'mingw32'
    c.poptions += -D_MT

  case 'windows'
  {
    # Note that upstream also passes -DOPENSSL_USE_APPLINK if compiled with
    # VC. We drop this option (see libcrypto/buildfile) for details.
    #
    c.poptions += -DOPENSSL_SYS_WIN32 -D_CRT_SECURE_NO_DEPRECATE \
                  -D_WINSOCK_DEPRECATED_NO_WARNINGS
  }
}

switch $c.class
{
  case 'gcc'
  {
    if ($c.target.cpu == 'i686')
      c.coptions += -fomit-frame-pointer

    # Disable warnings that pop up with -Wall -Wextra. Upstream doesn't seem
    # to care about these and it is not easy to disable specific warnings in a
    # way that works across compilers/versions (some -Wno-* options are only
    # recognized in newer versions). There are still some warnings left that
    # appear for certain platforms/compilers. We pass them through but disable
    # treating them as errors.
    #
    c.coptions += -Wno-all -Wno-extra -Wno-error
  }
  case 'msvc'
  {
    # Disable warnings that pop up with /W3.
    #
    c.coptions += /wd4090 /wd4244 /wd4267
  }
}

c.poptions =+ "-I$src_base/apps/include" "-I$src_base"

switch $tclass, $tsys
{
  case 'linux'
    c.libs += -ldl -pthread

  case 'bsd'
    c.libs += -pthread

  case 'windows', 'mingw32'
    c.libs += -lws2_32 -lgdi32 -lcrypt32

  case 'windows'
    c.libs += ws2_32.lib gdi32.lib crypt32.lib advapi32.lib
}
